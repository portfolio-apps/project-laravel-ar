#!/bin/bash
echo '----------------------------------------'
tput setaf 6; echo ' Building Docker Network...'; tput sgr 0;
echo '----------------------------------------'
cd ./
docker-compose up --build
echo ''
echo ''
