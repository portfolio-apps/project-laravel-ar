#!/bin/bash

###########################################################
echo '----------------------------------------'
tput setaf 6; echo ' Install Composer...'; tput sgr 0;
echo '----------------------------------------'
###########################################################
yarn docker:dev:cmd composer install

read -p "Would you like to migrate a fresh database? " -n 1 -r
echo  ''
if [[ $REPLY =~ ^[Yy]$ ]]
then
  bash ./docker/scripts/migrate.sh
fi
echo ''
echo ''

###########################################################
echo '----------------------------------------'
tput setaf 6; echo ' Building Frontend...'; tput sgr 0;
echo '----------------------------------------'
###########################################################
yarn docker:dev:cmd npm install && npm run watch
echo ''
echo ''
