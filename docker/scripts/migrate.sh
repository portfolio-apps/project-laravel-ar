#!/bin/bash
###########################################################
echo '----------------------------------------'
tput setaf 6; echo ' Setting up database...'; tput sgr 0;
echo '----------------------------------------'
###########################################################
cd ./
read -p "Would you like to seed the database? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
  yarn docker:dev:cmd php artisan migrate:fresh --seed
else
  yarn docker:dev:cmd php artisan migrate:fresh
fi
echo ''
echo ''
