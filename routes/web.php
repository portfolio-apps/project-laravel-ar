<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/viewer', function () {
//     return view('viewer');
// });

Route::get('/redis', function () {
    Redis::set('something', "THIS WORKED!");
    $keys = Redis::keys('*');
    $newArr = [];
    foreach($keys as $key) {
        $newArr[$key] = Redis::get($key);
    }
    // $value = Redis::get('cpu_temp:eb7cb373-af76-49f0-87bb-c614a21c46f9');
    return response()->json([
        'temp' => $value
    ]);
});

Route::get('/viewer', 'MarkerController@viewer_page');

Route::get('/markers/all', 'MarkerController@marker_page');
Route::get('/api/markers', 'MarkerController@index');

Route::get('/api/devices', 'DeviceController@index');