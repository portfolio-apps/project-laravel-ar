<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SwaggerScaffold extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'swagger:scaffold {model} {--method=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will scaffold a resource controller with Swagger PHP templates';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->option('method')) {
            $crud = [
                'index' => false,
                'create' => false,
                'show' => false,
                'update' => false,
                'delete' => false,
            ];

            $options = $this->option('method');

            foreach($options as $option => $value) {
                if ($value == 'index') {
                    $crud['index'] = true;
                } else if ($value == 'create') {
                    $crud['create'] = true;
                } else if ($value == 'show') {
                    $crud['show'] = true;
                } else if ($value == 'update') {
                    $crud['update'] = true;
                } else if ($value == 'delete') {
                    $crud['delete'] = true;
                }
            }
        }

        $model = (string) $this->argument('model');
        $lowercase = preg_split('/(?=[A-Z])/',$model);
        array_shift($lowercase);
        $lowercase = strtolower($lowercase[0]) . "_" . strtolower($lowercase[1]); // TODO: Fix this it will cause issues
        $file = "./app/Http/Controllers/" . $model ."sController.php";

        $source = file_get_contents($file);
        $tokens = token_get_all($source);

        $list_injectable = "/**
     * @OA\Get(
     *      path='/{$lowercase}s',
     *      security={{'token': {}}},
     *      operationId='list{$model}s',
     *      tags={'{$model}s'},
     *      summary='List all of the {$lowercase}s',
     *      description='Returns a list of {$lowercase}s',
     *      @OA\Response(
     *          response=200,
     *          description='Success Response',
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property='{$lowercase}',
     *                  type='array',
     *                  @OA\Items(
     *                      type='object',
     *                      ref='#/components/schemas/{$model}'
     *                  )
     *              ),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description='Unauthorized',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/Unauthenticated'
     *          )
     *      ),
     * )
     *";

        $post_injectable = "/**
     * @OA\Post(
     *      path='/{$lowercase}s',
     *      security={{'token': {}}},
     *      tags={'{$model}s'},
     *      operationId='create{$model}',
     *      summary='Create a {$model}',
     *      description='Creates a {$model} resource',
     *      @OA\RequestBody(
     *          request='create{$model}',
     *          description='{$model} Details',
     *          required=true,
     *          @OA\JsonContent(
     *              ref='#/components/schemas/{$model}PostReqBody'
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description='Successful Response',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/{$model}'
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description='Unauthorized',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/Unauthenticated'
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description='Bad Request',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/BadRequest'
     *          )
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description='You have filled in the neccessary body params',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/UnprocessableEntity'
     *          )
     *      ),
     *       @OA\Response(
     *          response=500,
     *          description='Internal Server Error',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/InternalServerError'
     *          )
     *      ),
     * )";
            
        $show_injectable = "/**
     * @OA\Get(
     *     path='/{$lowercase}s/{id}',
     *     security={{'token': {}}},
     *     tags={'{$model}s'},
     *     summary='Find a {$lowercase} by its ID',
     *     description='Show an single {$lowercase} object by its ID',
     *     operationId='show{$model}',
     *     @OA\Parameter(
     *         description='ID of the {$lowercase}',
     *         in='path',
     *         name='id',
     *         required=true,
     *         @OA\Schema(
     *           type='integer'
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description='Returns Single {$model}',
     *         @OA\JsonContent(
     *              ref='#/components/schemas/{$model}'
     *         )
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description='Unauthorized',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/Unauthenticated'
     *          )
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description='Resource Not Found',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/NotFound'
     *          )
     *     ),
     * ) 
     *";

        $put_injectable = "/**
     * @OA\Put(
     *     path='/{$lowercase}s/{id}',
     *     security={{'token': {}}},
     *     tags={'{$model}s'},
     *     operationId='update{$model}',
     *     summary='Update a {$lowercase} for an organization',
     *     description='Creates a {$model} resource',
     *     @OA\Parameter(
     *         description='ID of the {$lowercase}',
     *         in='path',
     *         name='id',
     *         required=true,
     *         @OA\Schema(
     *           type='integer'
     *           format='int64'
     *         )
     *     ),
     *     @OA\RequestBody(
     *         request='update{$model}',
     *         required=true,
     *         description='{$model} Details',
     *         @OA\JsonContent(
     *              ref='#/components/schemas/{$model}PostReqBody'
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description='Successful Response',
     *         @OA\JsonContent(
     *              ref='#/components/schemas/{$model}'
     *         )
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description='Unauthorized',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/Unauthenticated'
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description='Bad Request',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/BadRequest'
     *          )
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description='You have filled in the neccessary body params',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/UnprocessableEntity'
     *          )
     *      ),
     *       @OA\Response(
     *          response=500,
     *          description='Internal Server Error',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/InternalServerError'
     *          )
     *      ),
     * )
     *";

        $delete_injectable = "/**
     * @OA\Delete(
     *     path='/{$lowercase}s/{id}',
     *     security={{'token': {}}},
     *     summary='Delete an {$model} by its ID',
     *     description='Remove this {$lowercase} from your organization.',
     *     operationId='delete{$model}',
     *     tags={'{$model}s'},
     *     @OA\Parameter(
     *         description='ID of the {$lowercase}',
     *         in='path',
     *         name='id',
     *         required=true,
     *         @OA\Schema(
     *           type='integer'
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description='Successful Removal of Resource',
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description='Unauthorized',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/Unauthenticated'
     *          )
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description='Resource Not Found',
     *          @OA\JsonContent(
     *              ref='#/components/schemas/NotFound'
     *          )
     *     ),
     * ) 
     *";

        function addComment($comment, $injectable) {
        return str_replace('/**', str_replace("'", '"', $injectable), $comment);
        }

        foreach ($tokens as $token => $value) {

            if (isset($crud)) {

                if ($value[0] == 378 && strpos($value[1], 'Display a listing of the resource') == true && $crud['index'] == true) {
                    // LIST RESOURCES
                    $new_file = str_replace($value[1], addComment($value[1], $list_injectable), $source);
                } else if ($value[0] == 378 && strpos($value[1], 'Store a newly created resource in storage') == true && $crud['create'] == true) {
                    // CREATE RESOURCE
                    $new_file = str_replace($value[1], addComment($value[1], $post_injectable), ((isset($new_file)) ? $new_file : $source));
                } else if ($value[0] == 378 && strpos($value[1], 'Display the specified resource') == true && $crud['show'] == true) {
                    // SHOW RESOURCE
                    $new_file = str_replace($value[1], addComment($value[1], $show_injectable), ((isset($new_file)) ? $new_file : $source));
                } else if ($value[0] == 378 && strpos($value[1], 'Update the specified resource in storage') == true && $crud['update'] == true) {
                    // UPDATE RESOURCE
                    $new_file = str_replace($value[1], addComment($value[1], $put_injectable), ((isset($new_file)) ? $new_file : $source));
                } else if ($value[0] == 378 && strpos($value[1], 'Remove the specified resource from storage') == true && $crud['delete'] == true) {
                    // DELETE RESOURCE
                    $new_file = str_replace($value[1], addComment($value[1], $delete_injectable), ((isset($new_file)) ? $new_file : $source));
                }

            } else {

                if ($value[0] == 378 && strpos($value[1], 'Display a listing of the resource') == true) {
                    // LIST RESOURCES
                    $new_file = str_replace($value[1], addComment($value[1], $list_injectable), $source);
                } else if ($value[0] == 378 && strpos($value[1], 'Store a newly created resource in storage') == true) {
                    // CREATE RESOURCE
                    $new_file = str_replace($value[1], addComment($value[1], $post_injectable), ((isset($new_file)) ? $new_file : $source));
                } else if ($value[0] == 378 && strpos($value[1], 'Display the specified resource') == true) {
                    // SHOW RESOURCE
                    $new_file = str_replace($value[1], addComment($value[1], $show_injectable), ((isset($new_file)) ? $new_file : $source));
                } else if ($value[0] == 378 && strpos($value[1], 'Update the specified resource in storage') == true) {
                    // UPDATE RESOURCE
                    $new_file = str_replace($value[1], addComment($value[1], $put_injectable), ((isset($new_file)) ? $new_file : $source));
                } else if ($value[0] == 378 && strpos($value[1], 'Remove the specified resource from storage') == true) {
                    // DELETE RESOURCE
                    $new_file = str_replace($value[1], addComment($value[1], $delete_injectable), ((isset($new_file)) ? $new_file : $source));
                }
            }

        }

        $f=fopen($file,'w');
        fwrite($f, $new_file);
        fclose($f);

        $this->info($model . "Controller was scaffolded successfully!");
    }
}
